#!/bin/bash
yum update -y
sudo su – 
cd /usr/local/src
wget https://releases.hashicorp.com/terraform/0.12.0/terraform_0.12.0_linux_386.zip
unzip terraform_0.12.0_linux_386.zip
mv terraform /usr/local/bin/
export PATH=$PATH:/terraform-path/
