resource "aws_instance" "my-instance" {
	ami = "ami-0d5eff06f840b45e9"
	instance_type = "t2.nano"
	key_name = "Prod"
    subnet_id = "subnet-fa1917b0"
    vpc_security_group_ids = ["sg-0bf8f3e04deaf9eea"]
	#user_data = "file("install.sh")"
	user_data = "${file("install.sh")}"
	tags = {
		Name = "Terraform"	
		
	}
}
